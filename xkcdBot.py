#!/bin/python3
import telebot
import logging
import json
from random import randint
import tempfile
import urllib.request
import requests

#Logger per le attivita' del modulo telebot
logger = telebot.logger
telebot.logger.setLevel(logging.INFO)

#Imposto il token identificativo del bot
bot = telebot.TeleBot("132359703:AAH4CT3T8bKcFqRmTo5Yto5thKeJakhwXLc")

def sendMessage(chatId,url,desc,title):
	try:

		f = comicDL(url)

		if title or desc:	
			alt,title = getAlt(url)

		if title:
			bot.send_message(chatId, title)

		bot.send_photo(chatId, f)

		if desc:
			bot.send_message(chatId, alt)

		f.close()

	except Exception as e:
		print(e)

		bot.send_message(chatId,"Error")

def comicDL(url):
	
	try:
		dataJ =requests.get(url).json()

		src = dataJ['img']

		buffer = urllib.request.urlopen(src).read()
		f = tempfile.NamedTemporaryFile(suffix=src[len(src)-4:len(src)])
		f.write(buffer)
		f.seek(0)

		return f
	
	except Exception as e:
		print(e)

def getAlt(url):

	try:
		dataJ =requests.get(url).json()

		alt = dataJ['alt']
		title = dataJ['title']
		num = dataJ['num']

		title = "n. "+str(num)+": "+title

		return alt,title
	
	except Exception as e:
		print(e)

def randComic():

	try:
		dataJ =requests.get("http://xkcd.com/info.0.json").json()

		rand = randint(1,dataJ['num'])
	
		return rand

	except Exception as e:
		print(e)

@bot.message_handler(commands=['start', 'help'])
def welcome(message):
	
	bot.send_message(message.chat.id,"This is a unofficial xkcd bot.random - random comic\n/randomdesc - random comic with description\n/latest - latest comic\n/latestdesc - latest comic with description\n/number - comic number num\n/numberdesc - comic number num with description\n/help - help")

@bot.message_handler(commands=['random'])
def random(message):
	
	sendMessage(message.chat.id,"http://xkcd.com/"+str(randComic())+"/info.0.json",False,True)

@bot.message_handler(commands=['randomdesc'])
def randomdesc(message):

	sendMessage(message.chat.id,"http://xkcd.com/"+str(randComic())+"/info.0.json",True,True)

@bot.message_handler(commands=['latest'])
def latest(message):

	sendMessage(message.chat.id,"http://xkcd.com/info.0.json",False,True)

@bot.message_handler(commands=['latestdesc'])
def latestdesc(message):

	sendMessage(message.chat.id,"http://xkcd.com/info.0.json",True,True)
		
@bot.message_handler(commands=['number'])
def number(message):

	messaggio = message.text.replace("/number","").replace(" ","")
	try:
		sendMessage(message.chat.id,"http://xkcd.com/"+str(int(messaggio))+"/info.0.json",False,True)
	except Exception as e:
		print(e)
		bot.send_message(message.chat.id,"Error")

@bot.message_handler(commands=['numberdesc'])
def numberdesc(message):
	messaggio = message.text.replace("/numberdesc","").replace(" ","")
	try:
		sendMessage(message.chat.id,"http://xkcd.com/"+str(int(messaggio))+"/info.0.json",True,True)
	except Exception as e:
		print(e)
		bot.send_message(message.chat.id,"Error")
		
#Inizio a ricevere i messaggi, in caso di errore di ricezione il bot non si blocca grazie all'opzione none_stop
bot.polling(none_stop=True)
